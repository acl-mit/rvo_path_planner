import numpy as np
import pickle
from scipy import interpolate

class PosConverter(object):
	def __init__(self,cali_filename,cali_side_filename=None):
		# Read the calibration data file
		out = open(cali_filename, 'rU') #universal newline format - req'd for windows
		cali_data = pickle.load(out)
		out.close()
		
		# Initialize data
		self.proj_grid = cali_data['proj_grid']
		self.vicon_grid = cali_data['vicon_grid']
		self.window_size = cali_data['size']

		# Construct the linear interpolator
		self._to_vicon_x = interpolate.LinearNDInterpolator(self.proj_grid,self.vicon_grid[:,0])
		self._to_vicon_y = interpolate.LinearNDInterpolator(self.proj_grid,self.vicon_grid[:,1])
		self._to_proj_x = interpolate.LinearNDInterpolator(self.vicon_grid,self.proj_grid[:,0])
		self._to_proj_y = interpolate.LinearNDInterpolator(self.vicon_grid,self.proj_grid[:,1])

		self.use_side = False

		if cali_side_filename is not None:
			out = open(cali_side_filename, 'rU') #universal newline format - req'd for windows
			cali_data = pickle.load(out)
			out.close()

			# Initialize data
			self.proj_grid_side = cali_data['proj_grid']
			self.vicon_grid_side = cali_data['vicon_grid']
			self.window_size_side = cali_data['size']

			# Construct the linear interpolator
			self._to_vicon_x_side = interpolate.LinearNDInterpolator(self.proj_grid_side,self.vicon_grid_side[:,0])
			self._to_vicon_y_side = interpolate.LinearNDInterpolator(self.proj_grid_side,self.vicon_grid_side[:,1])
			self._to_proj_x_side = interpolate.LinearNDInterpolator(self.vicon_grid_side,self.proj_grid_side[:,0])
			self._to_proj_y_side = interpolate.LinearNDInterpolator(self.vicon_grid_side,self.proj_grid_side[:,1])
			self.use_side = True

	def _convert(self,proj_pos,con_x,con_y):
		proj_pos = np.atleast_2d(proj_pos)
		proj_pos_x = con_x(proj_pos)
		proj_pos_y = con_y(proj_pos)
		return np.array([proj_pos_x,proj_pos_y]).T.squeeze()

	def toViconPos(self,proj_pos):
		vicon_pos = self._convert(proj_pos,self._to_vicon_x,self._to_vicon_y)

		if not self.use_side:
			return vicon_pos
		else:
			vicon_pos_side = self._convert(proj_pos,self._to_vicon_x_side,self._to_vicon_y_side)
			nan_mask = np.isnan(vicon_pos)
			vicon_pos[nan_mask] = vicon_pos_side[nan_mask]
			return vicon_pos

	def toProjPos(self,vicon_pos):
		proj_pos = self._convert(vicon_pos,self._to_proj_x,self._to_proj_y)

		if not self.use_side:
			return proj_pos
		else:
			proj_pos_side = self._convert(vicon_pos,self._to_proj_x_side, self._to_proj_y_side)
			nan_mask = np.isnan(proj_pos)
			proj_pos[nan_mask] = proj_pos_side[nan_mask]
			return proj_pos

if __name__ == '__main__':
	conv = PosConverter('calibration_data.pickle','calibration_data_side.pickle')
	vicon_pos = [[0,0],[1,1],[-6,-4]]
	proj_pos = conv.toProjPos(vicon_pos)
	print 'vicon_pos: %s ---> proj_pos: %s' %(vicon_pos,proj_pos)
	print 'proj_pos: %s ---> vicon_pos: %s' %(proj_pos,conv.toViconPos(proj_pos))


