#include <ros/ros.h>
#include <vector>
#include "RVOPathPlanner.h"
#include "AgentTraj.h"
// #include "acl_msgs/Trajectory.h"
#include <path_planner/GenPath.h>
#include <path_planner/Trajectory.h>

class RVOWrapper{
public:
  // Parameters
  double neighborDist;
  int maxNeighbors;
  double timeHorizon;
  double timeHorizonObst;
  double radius;
  double maxSpeed;
  double goalThreshold;
  double simTimeStep;
  int maxSteps;
  double minRadius;
  int numIterate;

  // Methods
  RVOWrapper(){};
  ~RVOWrapper(){};

  void readParameters()
  {
    // Read from ROS parameters
    ros::param::getCached("~neighborDist",neighborDist);
    ros::param::getCached("~maxNeighbors",maxNeighbors);
    ros::param::getCached("~timeHorizon",timeHorizon);
    ros::param::getCached("~timeHorizonObst",timeHorizonObst);
    ros::param::getCached("~radius",radius);
    ros::param::getCached("~maxSpeed",maxSpeed);
    ros::param::getCached("~goalThreshold",goalThreshold);
    ros::param::getCached("~simTimeStep",simTimeStep);
    ros::param::getCached("~maxSteps",maxSteps);
    ros::param::getCached("~numIterate",numIterate);
    ros::param::getCached("~minRadius",minRadius);
  }


  path_planner::Trajectory getTrajMsg(RVO::AgentTraj& agentTraj)
  {
    path_planner::Trajectory traj;
    for (int i = 0; i < agentTraj.time_traj.size(); i++){
      float sim_time = agentTraj.time_traj[i];

      geometry_msgs::Vector3Stamped pos;
      geometry_msgs::Vector3Stamped vel;
      geometry_msgs::Vector3Stamped acc;
      geometry_msgs::Vector3Stamped jerk;

      pos.header.frame_id = "vicon";
      pos.header.stamp = ros::Time((double) sim_time);
      pos.vector.x = agentTraj.pos_traj[i].x();
      pos.vector.y = agentTraj.pos_traj[i].y();
      pos.vector.z = 0.0; //TODO check proper z.

      vel.header.frame_id = "vicon";
      vel.header.stamp = ros::Time((double) sim_time);
      vel.vector.x = agentTraj.vel_traj[i].x();
      vel.vector.y = agentTraj.vel_traj[i].y();

      acc.header.frame_id = "vicon";
      acc.header.stamp = ros::Time((double) sim_time);
      jerk.header.frame_id = "vicon";
      jerk.header.stamp = ros::Time((double) sim_time);

      traj.pos.push_back(pos);
      traj.vel.push_back(vel);
      traj.acc.push_back(acc);
      traj.jerk.push_back(jerk);
    }
    return traj;
  }

  bool genPathSrv(path_planner::GenPath::Request &req, path_planner::GenPath::Response &res){
    ros::Time startTime = ros::Time::now();
    RVO::RVOPathPlanner *planner = new RVO::RVOPathPlanner();
    readParameters();
    planner->setAgentParameters(neighborDist, maxNeighbors, timeHorizon, timeHorizonObst,
      radius, maxSpeed);
    planner->setSimParameters(simTimeStep, goalThreshold, maxSteps, minRadius);

    // Add agent
    for(int i = 0; i < req.N; i++){
      planner->addAgent(RVO::Vector2(req.p0[i].x,req.p0[i].y),
        RVO::Vector2(req.p1[i].x,req.p1[i].y));
    }
    // Add obstacle
    // TODO: use the obstacle from req
    std::vector<RVO::Vector2> obstacle1, obstacle2;
    obstacle1.push_back(RVO::Vector2(-2.6f,-3.7f));
    obstacle1.push_back(RVO::Vector2(-3.9f,-3.7f));
    obstacle1.push_back(RVO::Vector2(-3.9f,-5.0f));
    obstacle1.push_back(RVO::Vector2(-2.6f,-5.0f));
    obstacle2.push_back(RVO::Vector2(1.1f, 1.0f));
    obstacle2.push_back(RVO::Vector2(1.1f, -6.9f));
    obstacle2.push_back(RVO::Vector2(-7.5f, -6.9f));
    obstacle2.push_back(RVO::Vector2(-7.5f, -1.8f));
    obstacle2.push_back(RVO::Vector2(-2.0f, -1.8f));
    obstacle2.push_back(RVO::Vector2(-2.0f, 1.0f));
    planner->addObstacle(obstacle1);
    planner->addObstacle(obstacle2);

    // Generate paths using RVOPathPlanner
    bool reachGoalFlag = planner->genPath(numIterate);

    if (reachGoalFlag){
      ROS_INFO_STREAM("Goals reached.");
    }
    else{
      ROS_INFO_STREAM("Exceeding maximum steps.");
    }
    ROS_INFO_STREAM("Path Length: " << planner->agentTrajVec[0].pos_traj.size());
    ROS_INFO_STREAM("Radius: " << planner->radius);
    // Construct response
    res.converged = reachGoalFlag;

    for (int i = 0; i < planner->agentTrajVec.size(); i++){
      res.trajectories.push_back(getTrajMsg(planner->agentTrajVec[i]));
    }

    ROS_INFO_STREAM("Computation Time:" << ros::Time::now() - startTime);

    return true;
  }
};



int main(int argc, char **argv)
{
  ros::init (argc, argv, "rvo_path_planner");
  ros::NodeHandle n = ros::NodeHandle();

  // Set default parameters
  if (!ros::param::has("~neighborDist")) { ros::param::set("~neighborDist",15.0);}
  if (!ros::param::has("~maxNeighbors")) { ros::param::set("~maxNeighbors",15);}
  if (!ros::param::has("~timeHorizon")) { ros::param::set("~timeHorizon",1.0);}
  if (!ros::param::has("~timeHorizonObst")) { ros::param::set("~timeHorizonObst",0.2);}
  if (!ros::param::has("~radius")) { ros::param::set("~radius",0.15);}
  if (!ros::param::has("~maxSpeed")) { ros::param::set("~maxSpeed",0.8);}
  if (!ros::param::has("~goalThreshold")) { ros::param::set("~goalThreshold",0.1);}
  if (!ros::param::has("~simTimeStep")) { ros::param::set("~simTimeStep",0.1);}
  if (!ros::param::has("~maxSteps")) { ros::param::set("~maxSteps",200);}
  if (!ros::param::has("~numIterate")) { ros::param::set("~numIterate",1);}
  if (!ros::param::has("~minRadius")) { ros::param::set("~minRadius",0.1);}

  RVOWrapper rw;
  ros::ServiceServer srv_genPath = n.advertiseService("gen_path", &RVOWrapper::genPathSrv, &rw);

  ros::spin();

  return 0;
}