#include "AgentTraj.h"
// #include <ros/ros.h>

namespace RVO{
  int AgentTraj::log_data(float sim_time, RVO::Vector2 position, RVO::Vector2 vel, RVO::Vector2 pref_vel)
  {
    this->time_traj.push_back(sim_time);
    this->pos_traj.push_back(position);
    this->vel_traj.push_back(vel);
    this->pref_vel_traj.push_back(pref_vel);

    return time_traj.size();
  }
  
  void AgentTraj::set_agent_id(int id)
  {
    this->agent_id = id;
  }

  void AgentTraj::reset()
  {
    this->time_traj.clear();
    this->pos_traj.clear();
    this->vel_traj.clear();
    this->pref_vel_traj.clear();
  }

  bool AgentTraj::get_state_at_time(float t, Vector2& pos_out, Vector2& vel_out)
  {
    // Does interpolation.
    // Get index in trajectory according to current time
    std::vector<float>::iterator itr_next = std::upper_bound (time_traj.begin(), time_traj.end(), t);
    if (itr_next == time_traj.end()){
      // t is after the last point in trajectory, return the last point in traj.
      pos_out = pos_traj.back();
      vel_out = vel_traj.back();
      // acc_out = Vector2(0.0f,0.0f);
      return false;
    }
    else{
      if (itr_next == time_traj.begin()){
        // t is smaller than zero... 
        pos_out = pos_traj[0];
        vel_out = vel_traj[0];
        // acc_out = Vector2(0.0f,0.0f);
        return false;
      }
      else{
        int index_next = itr_next - time_traj.begin();
        int index_pre = index_next -1;

        float t_next = time_traj[index_next];
        float t_pre = time_traj[index_pre];
        float ratio = (t - t_pre)/(t_next - t_pre);
        pos_out = pos_traj[index_pre] + ratio*(pos_traj[index_next] - pos_traj[index_pre]);
        vel_out = vel_traj[index_pre] + ratio*(vel_traj[index_next] - vel_traj[index_pre]);
        // acc_out = normalize((vel_traj[index_next] - vel_traj[index_pre])/(t_next - t_pre));
        return true;
      }
    }
  }

}